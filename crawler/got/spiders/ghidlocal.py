# -*- coding: utf-8 -*-
import scrapy
from got.items import Event
import urllib.parse

class GhidlocalSpider(scrapy.Spider):
    name = 'ghidlocal'
    allowed_domains = ['ghidlocal.com']
    start_urls = ['https://ghidlocal.com/oradea/evenimente/lista']

    def parse(self, response):
        for element in response.css("div.type-tribe_events"):
            event = Event()
            event.set_all(None)

            event['title'] = element.css("a.tribe-event-url::text").extract_first().strip()
            event['preview'] = element.css("[itemprop='description']>p::text").extract_first()
            event['start_time'] = element.css("[itemprop='startDate']::attr(content)").extract_first()
            event['end_time'] = element.css("[itemprop='endDate']::attr(content)").extract_first()

            event['location_lon'] = element.css("span.tribe-street-address::text").extract_first() #location text
            
            classes = element.css("div.type-tribe_events::attr(class)").extract_first() #type
            if "category" in classes:
                event['type'] = [x for x in classes.split() if "category" in x][0][22:]
            
            url = element.css("a.tribe-events-read-more::attr(href)").extract_first() #description (text)
            event['url'] = url
            yield scrapy.Request(url, meta={"event": event}, callback=self.parse_details)


        next_page_url = response.css("li.tribe-events-nav-next > a::attr(href)").extract_first()
        if next_page_url is not None:
            yield scrapy.Request(url=next_page_url, callback=self.parse)

    def parse_details(self, response):
        event = response.meta["event"]
        event['text'] = "\n".join(response.css("div.tribe-events-single-event-description > p::text").extract())
        url = response.css("div.tribe-events-event-image > img::attr(src)").extract_first()
        if url:
            event['img_url'] = url
        yield event
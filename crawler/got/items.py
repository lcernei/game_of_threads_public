# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Event(scrapy.Item):
	title = scrapy.Field()
	preview = scrapy.Field()
	text = scrapy.Field()
	start_time = scrapy.Field()
	end_time = scrapy.Field()
	type = scrapy.Field()
	location_lon = scrapy.Field()
	location_lat = scrapy.Field()
	url = scrapy.Field()
	img_url = scrapy.Field()
	price = scrapy.Field()
	type = scrapy.Field()

	def set_all(self, value):
		for keys, _ in self.fields.items():
			self[keys] = value
from django.db import models
import datetime
from django.utils import timezone

# Create your models here.

class Event(models.Model):
	event_title = models.CharField(max_length=300)
	event_preview = models.TextField(max_length=500, default='event preview')
	event_text = models.TextField(default='event text')
	event_start_time = models.DateTimeField(null=True, blank=True) 
	event_end_time = models.DateTimeField(null=True, blank=True)
	event_type = models.CharField(max_length=100,default='event type')
	event_location_lon = models.FloatField(default='21.928030')
	event_location_lat = models.FloatField(default='47.055500')
	event_URL = models.URLField(default='event url')
	event_price = models.IntegerField(default=0)
	event_image_url = models.URLField(default='image url')

	def __str__(self):
		return self.event_title	
	
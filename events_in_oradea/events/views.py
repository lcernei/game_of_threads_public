from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404

from .models import Event

# Create your views here.
def index(request):
	events = Event.objects.all()
	# event_types = events.event_type
	return render(request, 'events/index.html', {'events': events})





def all_events(request):
	events = Event.objects.all()
	return render(request, 'events/all_events_template.html', {'events': events})





def event_entry(request,pk):
	events = Event.objects.all()
	event = get_object_or_404(Event, pk=pk)
	return render(request, 'events/event_entry_template.html', {'events': events, 'event': event})

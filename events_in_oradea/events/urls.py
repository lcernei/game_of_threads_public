from django.urls import path

from . import views

app_name = 'events'
urlpatterns = [
    path('', views.index, name='index'),
	path('all_events/', views.all_events, name='all_events_template'),
	path('<int:pk>/', views.event_entry, name='event_entry_template'),
]